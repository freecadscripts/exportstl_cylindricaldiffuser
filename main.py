import json

import createconicaldiffuser
import exportstl


f = open('parameters.json')
param = json.load(f)
f.close()

# create solid body
if os.path.exists(param["name"]["doc_0"] + '.FCStd'):
    pass
else:
    c = createconicaldiffuser.CreateConicalDiffuser()
    c.set_spreadsheet()
    c.create_tapered_cylinder()
    c.get_faces_on_which_sketch_will_be_created()
    c.create_pad_on_revolution('sketch_1', c.INDEX_MIN_X, 'diameter_1', 'pad_0', 'straight_inlet')
    c.create_pad_on_revolution('sketch_2', c.INDEX_MAX_X, 'diameter_2', 'pad_1', 'straight_outlet')
    c.trim_three_fourths_of_whole_body()
    c.save_document()

# 1. downgrade the body into faces
# 2. create surface meshes from the faces
# 3. export the meshes as stl file
e = exportstl.ExportSTL()
e.get_parameters()
e.set_facelabel()
e.export_stl()
e.save_document()