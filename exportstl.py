import json
import os
from re import S
import subprocess
import FreeCAD as App
import Part

class ExportSTL:
    PWD = os.getcwd()

    def __init__(self):
        f = open('parameters.json')
        self.param = json.load(f)
        f.close()
        
        file_path = self.PWD + '/' + self.param["name"]["doc_0"] + '.FCStd'
        
        self.doc_0 = App.open(file_path)
        self.sheet_0 = self.doc_0.getObject(self.param["name"]["sheet_0"])
        self.body_0 = self.doc_0.getObject(self.param["name"]["body_0"])

        return


    def get_parameters(self):
        self.X_INLET = self.sheet_0.get('x_inlet')
        self.X_WALL_UP = self.sheet_0.get('x_wall_up')
        self.X_WALL_DOWN = self.sheet_0.get('x_wall_down')
        self.X_OUTLET = self.sheet_0.get('x_outlet')
        
        return


    def set_facelabel(self):
        TOLERANCE = 1.0e-4  # [mm]

        faces = self.body_0.Shape.Faces
        faces_wall = []
        faces_symmetry = []

        for face in faces:
            x = face.CenterOfMass.x
            y = face.CenterOfMass.y
            z = face.CenterOfMass.z
            if abs(y) < TOLERANCE:
                faces_symmetry.append(face)
            elif abs(z) < TOLERANCE:
                faces_symmetry.append(face)
            elif abs(x - self.X_INLET) < TOLERANCE:
                face_inlet = face
            elif abs(x - self.X_WALL_UP) < TOLERANCE:
                face_wall_up = face
            elif abs(x - self.X_WALL_DOWN) < TOLERANCE:
                face_wall_down = face
            elif abs(x - self.X_OUTLET) < TOLERANCE:
                face_outlet = face
            else:
                faces_wall.append(face)

        face_wall = Part.Compound(faces_wall)
        face_symmetry = Part.Compound(faces_symmetry)

        self.renamed_faces = {
            'inlet': face_inlet, 
            'outlet': face_outlet, 
            'wall_upstream_of_taper': face_wall_up,
            'wall_downstream_of_taper': face_wall_down, 
            'wall_others': face_wall, 
            'symmetry': face_symmetry
        }

        return


    def export_stl(self):    
        subprocess.run(["mkdir stl"], shell=True) # !!! wanna modify with self.PWD

        for key in self.renamed_faces.keys():
            self.renamed_faces[key].exportStl(self.PWD + '/stl/' + key + '.ast')
        
        subprocess.run(["ls stl/ | sed 's/\.ast//' | xargs -I@ mv stl/@.ast stl/@.stl"], shell=True)
        
        return


    def save_document(self):
        pwd = os.getcwd()
        file_path_0 = pwd + "/" + self.param["name"]["doc_0"] + ".FCStd"
        self.doc_0.saveAs(file_path_0)

        return
