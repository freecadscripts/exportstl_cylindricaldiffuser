# STLファイルの出力: 円筒ディフューザ

作成: M.TANAKA
日付: 2022.09.18

[TOC]

## 1. 概要

- OpenFOAMで任意の形状を解析するには、その形状をSTLで作成する**プリプロセッサが必要**です。外部流れの場合は１つのソリッドを１つのSTLファイルに出力すれば良いので、後は例えば[XSim](https://xsim.work/)などで簡便にケースフォルダを準備することもできます。

- しかし内部流れやマルチリージョン流れなど、より複雑なジオメトリの流れを解析するには、結局、**１つのソリッドから領域境界ごとに複数のSTLファイルを出力する**必要が出てきます。

- これも数ケース程度であれば手作業でCADを操作すれば良いですが、パラメータスタディなどをしたい場合、**作業時間がネックになります**。たいていのCADはマクロ機能を持っているので、「ソリッド1種類につき１クリック」までは削減できますが、それでもN種類用意するのにN回の操作が必要です。

- CADソフトの中でも**FreeCADは、Pythonスクリプトを用いてコンソールから実行することができます**。スクリプトの中でループを設ければ、N種類のソリッドを１回のEnterで用意することが可能であり、便利です。

- ここでは、FreeCADを用いて**内部流れ解析のためのSTLファイルを作成するスクリプト**を紹介します。


## 2. 最終的なアウトプット

こんなSTLファイルが得られます。
- ツリー：
    ```
    stl
    ├── inlet_SI.stl
    ├── outlet_SI.stl
    ├── symmetry_SI.stl
    ├── wall_downstream_of_taper_SI.stl
    ├── wall_others_SI.stl
    └── wall_upstream_of_taper_SI.stl
    ```

- `inlet_SI.stl`の中身：
    ```
    solid solid
     facet normal -1 0 0
      outer loop
       vertex -0.075 1.7e-17 -2.1e-17
       vertex -0.075 0.0353553 0.0353553
       vertex -0.075 0.0362998 0.034385
      endloop
     endfacet
     ...
     facet normal -1 0 -0
      outer loop
       vertex -0.075 3e-17 0.05
       vertex -0.075 0.00135397 0.0499817
       vertex -0.075 1.7e-17 -2.1e-17
      endloop
     endfacet
    endsolid solid
    ```

ParaViewで可視化するとこんな感じです:  

![](readme_images/exported_stl.png)

計算領域の境界としてのSTLと、メッシュ密度を制御するリージョンとしてのSTLの両方を作成しています。
| 色      | 境界名                   |
| :-----: | :-----------------------:|
| 白(灰)  | inlet                    |
| (裏側)  | outlet                   |
| 緑      | symmetry                 |
| ピンク  | wall_upstream_of_taper   |
| 青      | wall_downstream_of_taper |
| 黃      | wall_others              |

※ピンクと青のパッチは、ディフューザの損失係数を計算するときの参照箇所として他の壁面から区別しました。


## 3. 使い方

### 3.1 ソフトウェア要件

- **Linux** (WSLでもOK。ディストロは何でも良いが、Ubuntu22.04以外では検証してません)
- [**Anaconda**](https://docs.anaconda.com/anaconda/install/linux/)
    - [**FreeCAD**](https://anaconda.org/conda-forge/freecad) (v0.20を使用)
- [**OpenFOAM**](https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled) (v2206を使用)

**FreeCADはAnacondaを使ってインストール**しています。OSやディストロに関係なく同じバージョンを使うことができるので面倒がないです。

### 3.2 Anaconda仮想環境の作成

AnacondaでFreeCAD用の仮想環境を新規に用意します。

#### 3.2.1 新規環境の作成

Anacondaはインストール済みとします。ターミナルを開き、下記コマンドで`opencae`という名前の環境を新規作成します。名前は任意です。2022年11月時点でFreeCADはPython3.11に対応していないので、古いバージョンを指定します。
```
(base)$ conda create -n opencae python=3.10
```

作成した環境を有効化します。いちいち打つのが面倒なので私は`~/.bashrc`に追記しています。
```
(base)$ conda activate opencae
(opencae)$ 
```

#### 3.2.2 FreeCADのインストール

仮想環境`opencae`にFreeCADをインストールします。
```
(opencae)$ conda install -c conda-forge freecad
```
下記コマンドで起動します。※すべて小文字です。
```
(opencae)$ freecad
```

| ![no image!](readme_images/freecadcmd.png) |
| :----------------------------------------: |
| 起動直後のFreeCADウィンドウ                  |

### 3.3 実行方法

1. このプロジェクトフォルダをダウンロードして、適当な場所に展開します。
2. ターミナルを開いて`./run.bash`と打てば、STLファイルが生成されます。
3. フォルダを初期化するには`./clean.bash`と入力します。

## 4. 解説: GUI操作とCUI操作

ここでは、通常のGUI操作の方法には言及しません。そうしたFreeCADの使い方については、例えば下記youtube動画などが参考になるでしょう。

- [**Mango Jelly Solutions, FreeCAD 0.20 For Beginners**](https://www.youtube.com/watch?v=NXN7TOg3kj4&list=PLWuyJLVUNtc0UszswD0oD5q4VeWTrK7JC)

FreeCADをインストールすると、２つのソフトが使えるようになります：

- **FreeCAD**（区別するため、GUI版FreeCADと呼ぶことにします）
    - 上の画像に示した、GUIで操作するCADソフトです。FreeCADの解説動画では基本的にこちらしか取り上げられません。

- **FreeCADCmd**
    - ターミナルから操作するCUIソフトです。起動するとPythonコンソールが現れます。ここにコマンドを入力することでCADを操作します。以下は、起動直後のFreeCADCmdの様子です。

        ```
        (opencae) $ freecadcmd
        FreeCAD 0.20.1, Libs: 0.20.1R29410 (Git)
        (c) Juergen Riegel, Werner Mayer, Yorik van Havre and others 2001-2022
        FreeCAD is free and open-source software licensed under the terms of LGPL2+ license.
        FreeCAD wouldn't be possible without FreeCAD community.
        #####                 ####  ###   ####  
        #                    #      # #   #   # 
        #     ##  #### ####  #     #   #  #   # 
        ####  # # #  # #  #  #     #####  #   # 
        #     #   #### ####  #    #     # #   # 
        #     #   #    #     #    #     # #   #  ##  ##  ##
        #     #   #### ####   ### #     # ####   ##  ##  ##

        [FreeCAD Console mode <Use Ctrl-D (i.e. EOF) to exit.>]
        >>> 
        ```

    - GUI版に対する利点は、`.py`ファイルにスクリプトを記述すれば、`$ freecadcmd foobar.py`のようにコマンド１つでCAD操作を実現できることです。OpenFOAMであれば、形状のパラメータ最適化などのように、自動で何パターンもCADを作成したい場合に活きる機能です。

ここではFreeCADCmdでOpenFOAMの入力STLファイルを作成する方法を示します。

### 4.1 Pythonコンソール

FreeCADの優れているのは、GUI版FreeCADにも「**Pythonコンソール**」というものが用意されている点です。これのおかげで、GUI操作をPythonスクリプトに置き換える作業がかなり楽になります。

Pythonコンソールを表示するには、まずGUI版FreeCADを起動し、上部タブから「表示」→「パネル」→「Pythonコンソール」をチェックします。ウィンドウ下部にPythonコンソールが確認できるはずです。

![no image!](readme_images/PythonConsole.png)

### 4.2 GUI操作に対応するPythonコマンドの確認

Pythonコンソールに表示されるコマンドは、GUI操作と対応しています（一部コードに現れない操作もあります）。例えば、GUI版FreeCADを起動してすぐの状態で、上部タブから「ファイル」→「新規」をクリックすると、**コンソールには次のようなコマンドが新たに表示されます**。コメント行以外の3行のうち、1行目は新規ドキュメント作成、2，3行目は視点設定を行っています。

```python
### Begin command Std_New
App.newDocument("Unnamed")
# App.setActiveDocument("Unnamed")
# App.ActiveDocument=App.getDocument("Unnamed")
# Gui.ActiveDocument=Gui.getDocument("Unnamed")
Gui.activeDocument().activeView().viewDefaultOrientation()
### End command Std_New
Gui.runCommand('Std_OrthographicCamera',1)
```

- **余談** 
    - `App`, `Gui`はそれぞれ`FreeCAD`, `FreeCADGui`というPythonライブラリのエイリアスです。`Gui`は名前の通りGui操作に関わる機能のみを集めたもので、FreeCADCmdでは利用できません（その必要がありません）。したがって、`Gui`については考えなくて良いです。
    - じゃあなぜ`Gui`があるかというと、FreeCADCmdとは別に、GUI版FreeCAD上で動作する「マクロ」という機能があるからです。マクロでは、視点移動や表示非表示などの視覚効果もスクリプト化することが可能です。

逆に、**Pythonコンソールにコマンドを打ち込むことで、GUI操作と同じことを実現できます**。GUI版FreeCADを起動し直して、Pythonコンソールに以下を打ち込みます。`foo`という名前のドキュメントが新規作成されるはずです。

```python
App.newDocument("foo")
```

このように、Pythonコンソールは**対話的に操作できる**ので、GUI操作とAPIとの対応を理解しやすいです。

### 4.3 Python APIの探り方

Python APIをうまく使えば、**GUI操作ではできないことも可能になります**。GUI操作では出てこないAPIについて知るには、ドキュメントを読むという手もあります（「ヘルプ」→「Pythonモジュールのマニュアル」）が、Pythonコンソールでオブジェクトのメンバを探ったほうが早いです。ここではそのやり方を例で示します。

#### 4.3.1 お題となるソリッドを作成する

まず、下記スクリプトをPythonコンソールで1行ずつ実行しましょう。barという名前の円柱が生成されるはずです。

```python
App.newDocument("foo")
doc = App.ActiveDocument
doc.addObject("Part::Cylinder","cylinder")
cylinder = doc.ActiveObject
cylinder.Label = "bar"
doc.recompute()
```

- **余談**
    上記スクリプトは次のように短く書けます(Pythonコンソールで動作することを確認してみましょう)。Pythonコンソールに自動表示されるコマンドは結構冗長なので、スクリプト化する際は色々試して推敲すると良いです。
    ```python
    doc = App.newDocument("foo")    
    cylinder = doc.addObject("Part::Cylinder","cylinder")
    cylinder.Label = "bar"
    doc.recompute()
    ```
#### 4.3.2 ソリッドの特定の面をGUIでエクスポートする(失敗)

これの上面だけをSTLファイルとしてエクスポートしたいとします（inlet, outlet, wallを分けてSTL化する場合を想定しています）。実はGUI操作ではこれはできません。実際、以下のように

| ![](readme_images/activeObject.png) | ![no image!](readme_images/exportActiveObject.png) |
| :---------------------------------: | :------------------------------------------------: |
| 1. グラフィック上で上面を選択し       | 2.「ファイル」→「エクスポート」を選択。                 |

| ![no image!](readme_images/exportStl.png) | `$ mv top.ast top.stl` |
| :---------------------------------------: | :--------------------: |
| 3. ast形式で保存(OpenFOAMはbinary読めない)  | 4. 拡張子を修正         |

としても、`top.stl`には円柱全体のデータが保存されます（ParaView等で確認してみましょう）。

#### 4.3.3 ソリッドの特定の面をPython APIでエクスポートする

では、上面のみのエクスポートをPythonコンソールで実現するにはどうすれば良いでしょうか。PythonコンソールでのFreeCADの操作は、大まかに言って

1. ライブラリ関数 (例: ライブラリ`App`の`newDocument()`)
2. オブジェクトのメンバ関数 (これから示します)

の2種類の関数を使って実現します。**これらの関数のリストは、Pythonコンソールで確認できます**。Pythonコンソールで`App.`や`cylinder.`と入力してみましょう。メンバのリストが現れます。このリストから、STLをエクスポートする関数を探し出します。答えを先にいうと、次のようにします。

| ![no image!](readme_images/menbersOfCylinder.png) | ![no image!](readme_images/member1_Shape.png) ![no image!](readme_images/member2_exportStl.png) |
| :---------------------------------------: | :--------------------: |
| 1. `cylinder.`と打つと…  | 2. サブオブジェクト`Shape`を選び…         |

このように、`cylinder.Shape.exportStl("/home/tanaka/Documents/foo-bar.ast")`と打つと、円柱全体のastファイルがエクスポートされます。(ユーザ名は適宜修正してください)

次に、上面のみに対して同じことをします。リストをしらみつぶしに試行錯誤してもらうのが良いですが、ここでは答えだけ示します。

```python
>>> faces = cylinder.Shape.Faces
>>> len(faces)
3 # facesは３つの面で構成されている
>>> faces[0].CenterOfMass.z
5.0
>>> faces[1].CenterOfMass.z
0.0
>>> faces[2].CenterOfMass.z
10.0 # 重心のz座標が最も大きい = face[2]が上面
>>> faces[2].exportStl("/home/tanaka/Documents/top.ast") # 'tanaka'は適宜修正
```

あとは`$ mv top.ast top.stl`で拡張子を直します。ParaViewで確認すれば、確かに上面だけがエクスポートされているのがわかります。

![](readme_images/top_stl.png)

このとき、対話的な操作によって`faces[2]`が上面であることを特定していますが、実際にスクリプトにする場合この手は使えません。重心のz座標をリストにして、最大値のインデックスからfaceを特定する必要があります。そのあたりの手法はこのリポジトリのコードで確認してみてください。


## 5. このプロジェクトのUML図

シーケンス図(書式は適当です)：

```plantuml
== "script_pre.py" ==
run.bash -> ConicalDiffuser : $ freecadcmd script_pre.py
ConicalDiffuser -> ConicalDiffuser : __init__()
ConicalDiffuser -> ConicalDiffuser : set_spreadsheet()
ConicalDiffuser -> ConicalDiffuser : create_tapered_cylinder()
ConicalDiffuser -> ConicalDiffuser : get_faces_on_which_sketch_will_be_created()
ConicalDiffuser -> ConicalDiffuser : create_pad_on_revolution()
ConicalDiffuser -> ConicalDiffuser : create_pad_on_revolution()
ConicalDiffuser -> ConicalDiffuser : trim_three_fourths_of_whole_body()
ConicalDiffuser -> ConicalDiffuser : save_document()
ConicalDiffuser -> run.bash

== "script.py" ==
run.bash -> ExportSTL : $ freecadcmd script.py
ExportSTL -> ExportSTL : __init__()
ExportSTL -> ExportSTL : get_parameters()
ExportSTL -> ExportSTL : set_facelabel()
ExportSTL -> ExportSTL : export_stl()
ExportSTL -> ExportSTL : save_document()
ExportSTL -> run.bash

run.bash -> run.bash : (scaling STL files by 'surfaceConvert')
```


## 6. このスクリプトでやっていること

実際に動かしてみると一番理解が早いと思います。5章のシーケンス図に見える関数がスクリプトのmain文に並んでいますので、関数をコメントアウトして上の方から徐々に実行して、その段階ごとにFCStdファイルをGUI版FreeCADで確認すると、だいたい起こっていることが分かると思います。

以下には補足として、要素的にピックアップしたコードの役割を説明します。

### 6.1 スプレッドシートの作成

jsonファイルから読み込んだディフューザの形状パラメータをスプレッドシートに写し、加えて従属的なパラメータを計算させます。
直接スケッチにパラメータを与えずスプレッドシートを経由させることで、GUI上で対話的にパラメータを変更できるようになります。自動化という観点では不要ですが、後々パラメータの意味を見返す際に分かりやすいはずです。

![](readme_images/spreadsheet.png)

下の画像のようにウィンドウを並べた上で、スプレッドシートの独立変数を変更してみてください。ソリッドが再計算されて形状が変化するはずです。

| ![](readme_images/changeParameter.png)         |
| :--------------------------------------------: |
| 「ウィンドウ」→「並べて表示」でこのように表示できる。<br>※ソリッドが表示されない場合は、ツリーで選択してSpaceキーを押す(アイコンが灰色→青に変わる)。 |

```python
    ### cell color parameters
    BLUE = (0.5, 1.0, 1.0)
    GRAY = (0.8, 0.8, 0.8)

    def set_spreadsheet(self):
        self.sheet_0.set('A1', 'Parameter') ### セルの値を設定するAPI関数。A1セルに'Parameter'を入力。
        self.sheet_0.set('B1', 'Value')
        self.sheet_0.set('C1', 'Unit')
        self.sheet_0.set('D1', 'IsIndependent')

        expression = self.param["geometry"]["diameter_1"] ### 自作jsonファイルから読み込んだパラメータ
        self.set_parameter(self.sheet_0, '2', 'diameter_1', expression, 'mm', 'True')

        expression = '= 1.5 * diameter_1'
        self.set_parameter(self.sheet_0, '3', 'diameter_2', expression, 'mm', 'False')
        ...
        self.sheet_0.recompute()
                       
        return

    def set_parameter(self, sheet, row, name, expression, unit, is_indepemdent):
        ### 説明のために、
        ###     row            = 3
        ###     name           = 'diameter_2'
        ###     expression     = '= 1.5 * diameter_1'
        ###     unit           = 'mm'
        ###     is_indepemdent = 'False'
        ### とする。
        sheet.set('A' + row, name)
        sheet.set('B' + row, expression)
            ### Excel等と同様に、セルの値は式で与えることができる。
            ### 式中のセルはエイリアス(後述)で指定することができるので、
            ### '= 1.5 * B2' を '= 1.5 * diameter_1' と書けて可読性が良くなる。
        sheet.setAlias('B' + row, name) 
            ### B3セルに'diameter_2'というエイリアスを設定するAPI関数。
            ### これにより別のAPI関数を用いてsheet.get('diameter_2')という風にB3セルの値を取得できる。
        sheet.set('C' + row, unit)
        if is_indepemdent == 'True':
            sheet.set('D' + row, 'True')
            sheet.setBackground('D' + row, self.BLUE)
                ### sheet.setBackground()はセルを塗りつぶすAPI関数。
                ### 独立パラメータと従属パラメータを一目で判別できるようにしている。 
        else:
            sheet.set('D' + row, 'False')
            sheet.setBackground('D' + row, self.GRAY)

        return
```

### 6.2 スケッチの作成

![](readme_images/sketch.png)

```python
    ### sketch parameters
    START  = 1
    END    = 2
    CENTER = 3
    X_AXIS = -1
    Y_AXIS = -2

    def create_tapered_cylinder(self):
        sketch_0 = self.body_0.newObject('Sketcher::SketchObject', 'sketch_0')
        sketch_0.Support = (self.doc_0.getObject('XY_Plane'), [''])
        sketch_0.MapMode = 'FlatFace'

        p0 = App.Vector(  0.0, 25.0, 0.0) ### 位置ベクトル
        p1 = App.Vector( -5.0, 25.0, 0.0)
        p2 = App.Vector( -5.0,  0.0, 0.0)
        p3 = App.Vector(100.0,  0.0, 0.0)
        p4 = App.Vector(100.0, 50.0, 0.0)
        p5 = App.Vector( 95.0, 50.0, 0.0)

        geo_0 = []
        # index 0 ~ 4
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p0, p1), False))
            ### line = Part.LineSegment(p1, p2) は２つの座標を結ぶ有向線分。line.StartPoint, line.EndPoint で p0, p1 を取得できる。
            ### sketch_0.addGeometry(line, False) でスケッチ上に線分を追加する。このとき、整数の戻り値を受け取る。この値でスケッチ上の線分を識別する。
            ### したがって geo_0[i] にはintが格納される（更にいうと geo_0[i] = i である）。
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p1, p2), False))
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p2, p3), False))
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p3, p4), False))
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p4, p5), False))
        # index 5
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p5, p0), False))

        con_0 = []
        ### 以下で拘束を設定するためにaddConstraint()するたびに、整数が0から順に返さる。FreeCADはこの整数で拘束を識別するので、con_0に返り値を格納している。(con_0[i] = i なので、あまり意味はないかも。より可読性の高い書き方があれば変更したい。)
        # index 0
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Coincident',    geo_0[0], self.END, geo_0[1], self.START)))
            ### geo_0[0]に対応する線分の終点と、geo_0[1]に対応する線分の始点が一致するという拘束。
            ### FreeCADの素朴な記法だと addConstraint(Sketcher.Constraint('Coincident', 0, 2, 1, 1)) となり、
            ### 数字の意味が分かりづらいのでエイリアスとして配列 geo_0[] や定数 START, END, X_AXIS, Y_AXIS を導入した。
        ...
        # index 9
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Vertical',      geo_0[3]))) ### 線分の垂直拘束
        # index 10 ~ 14
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Horizontal',    geo_0[4]))) ### 線分の水平拘束
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('PointOnObject', geo_0[0], self.START, self.Y_AXIS))) ### 線分の始点がY軸上にあるという拘束
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('PointOnObject', geo_0[1], self.END,   self.X_AXIS))) ### 線分の終点がX軸上にあるという拘束
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('DistanceY',     geo_0[1], self.END,   geo_0[1], self.START, 25.0))) # ２点間の距離拘束。１つ目の点より２つ目の点の方がY座標が大きくなる。
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('DistanceY',     geo_0[3], self.START, geo_0[3], self.END,   50.0)))
        # index 15 ~ 17
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('DistanceX',     geo_0[0], self.END,   geo_0[0], self.START,  5.0))) # ２点間の距離拘束。１つ目の点より２つ目の点の方がX座標が大きくなる。
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('DistanceX',     geo_0[4], self.END,   geo_0[4], self.START,  5.0)))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Angle',         geo_0[2], self.START, geo_0[5], self.END,    0.31416))) # 角度拘束。単位はラジアン。

        # note: constraint of DIAMETER_2 MUST BE defined before that of DIAMETER_1
        #       because DIAMETER_2 > DIAMETER_1. (Or the sketch solver may fail to recompute().)
        sht = '<<' + self.param["name"]["sheet_0"] + '>>.'
        sketch_0.setExpression('Constraints[' + str(con_0[14]) + ']', '0.5 * ' + sht + 'diameter_2')
            ### 変数展開すると ('Constraints[14]', '0.5 * <<Parameters>>.diameter_2')
            ### スプレッドシートのdiameter_2と結びつけている。
            ### sketch_0.Constraints[] はAPIで用意されたリスト。要素の型は Sketcher.Constraint
            ### Constraints[14]はcon_0[14]で与えた拘束に対応する。
        sketch_0.setExpression('Constraints[' + str(con_0[13]) + ']', '0.5 * ' + sht + 'diameter_1')
        sketch_0.setExpression('Constraints[' + str(con_0[15]) + ']', sht + 'small_length')
        sketch_0.setExpression('Constraints[' + str(con_0[16]) + ']', sht + 'small_length')
        sketch_0.setExpression('Constraints[' + str(con_0[17]) + ']', '0.5 * ' + sht + 'taper_angle')

        self.doc_0.recompute()
        ...
        return
```

### 6.3 ソリッドの作成

| ![](readme_images/revolution.png) | ![](readme_images/pad.png) |
| :-------------------------------: | :------------------------: |
| Revolution                        | Pad                        |

```python
    def create_tapered_cylinder(self):
        ...

        # create a solid from the sketch
        self.revolution_0 = self.body_0.newObject('PartDesign::Revolution', 'revolution_0') ### 回転押出オブジェクト
        self.revolution_0.Profile = sketch_0 ### 押出に使うスケッチを選択
        self.revolution_0.ReferenceAxis = (self.doc_0.getObject('X_Axis'), ['']) ### 回転軸を選択
        self.revolution_0.Angle = 360.0 ### 回転角度を設定（一回転）

        self.doc_0.recompute()

        return


    def create_pad_on_revolution(self, SKETCH_LABEL, FACE_INDEX, name_diameter, PAD_LABEL, name_length):
        ...

        # Begin command PartDesign_Pad
        pad = self.body_0.newObject('PartDesign::Pad', PAD_LABEL) ### 押出オブジェクト
        pad.Profile = sketch ### 押出に使うスケッチを選択
        pad.setExpression('Length', sht + name_length) ### 押出長さをスプレッドシートの値から設定
        pad.ReferenceAxis = (sketch, ['N_Axis']) ### 押出方向を選択（スケッチに垂直）

        self.doc_0.recompute()

        return
```

### 6.4 ソリッドからフェイスの抽出

フェイスを特定するために、`Part.Face`クラスの`CenterOfMass`というメンバ変数を用いています。

```python
    def set_facelabel(self):
        TOLERANCE = 1.0e-4  # [mm] フェイス位置の一致を判定するときに使う微小な値。

        faces = self.body_0.Shape.Faces ### body_0のフェイスのリスト
        faces_wall = [] ### 壁面パッチのリスト
        faces_symmetry = [] ### 対称パッチのリスト

        for face in faces:
            x = face.CenterOfMass.x ### フェイスの重心座標
            y = face.CenterOfMass.y
            z = face.CenterOfMass.z
            if abs(y) < TOLERANCE: ### y == 0.0 とすると検出漏れが生じるため、こう書いている
                faces_symmetry.append(face)
            elif abs(z) < TOLERANCE:
                faces_symmetry.append(face)
            elif abs(x - self.X_INLET) < TOLERANCE:
                face_inlet = face
            elif abs(x - self.X_WALL_UP) < TOLERANCE:
                face_wall_up = face
            elif abs(x - self.X_WALL_DOWN) < TOLERANCE:
                face_wall_down = face
            elif abs(x - self.X_OUTLET) < TOLERANCE:
                face_outlet = face
            else:
                faces_wall.append(face)

        face_wall = Part.Compound(faces_wall) ### リストを一つのCompoundオブジェクトに統合
        face_symmetry = Part.Compound(faces_symmetry)

        ### フェイスの名前とオブジェクトのディクショナリを作成
        self.renamed_faces = {
            'inlet': face_inlet, 
            'outlet': face_outlet, 
            'wall_upstream_of_taper': face_wall_up,
            'wall_downstream_of_taper': face_wall_down, 
            'wall_others': face_wall, 
            'symmetry': face_symmetry
        }

        return
```

### 6.5 ソリッド/フェイスのSTLエクスポート

STLファイルを出力するために、`Part.Face`, `Part.Compound`クラスに共通の`exportStl()`というメンバ関数を用いています。

```python
    def export_stl(self):    
        subprocess.run(["mkdir stl"], shell=True) ### bashでフォルダstlを作成

        for key in self.renamed_faces.keys():
            self.renamed_faces[key].exportStl(self.PWD + '/stl/' + key + '.ast')
                ### フルパスで指定（相対パスでもよい）。ast形式でないとOpenFOAMで読み込めない。
        
        subprocess.run(["ls stl/ | sed 's/\.ast//' | xargs -I@ mv stl/@.ast stl/@.stl"], shell=True)
            ### bashでファイル拡張子をastからstlに書き換える

        return
```

## 7. 関連するプロジェクト

### 7.1 派生元:

- https://gitlab.com/freecadscripts/partdesign_cylindricaldiffuser


### 7.2 派生先:

- ~~https://gitlab.com/openfoamscripts/fundamentals/cylindricaldiffuser~~
(under construction)


## 8. 参考

- https://wiki.freecadweb.org/Scripting_and_macros
    - https://wiki.freecadweb.org/Scripts
    - https://wiki.freecadweb.org/Sketcher_scripting
    - https://wiki.freecadweb.org/Mesh_Scripting
