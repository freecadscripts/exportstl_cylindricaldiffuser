import json
import os
import numpy as np
import FreeCAD as App
import Part
import Sketcher


class CreateConicalDiffuser:
    ### cell color parameters
    BLUE = (0.5, 1.0, 1.0)
    GRAY = (0.8, 0.8, 0.8)

    ### sketch parameters
    START  = 1
    END    = 2
    CENTER = 3
    X_AXIS = -1
    Y_AXIS = -2

    def __init__(self):  
        f = open('parameters.json')
        self.param = json.load(f)
        f.close()

        print(self.param["name"]["doc_0"])

        self.doc_0 = App.newDocument(self.param["name"]["doc_0"])
        self.sheet_0 = self.doc_0.addObject('Spreadsheet::Sheet', self.param["name"]["sheet_0"])
        self.body_0 = self.doc_0.addObject('PartDesign::Body', self.param["name"]["body_0"])
        
        return


    def set_spreadsheet(self):
        self.sheet_0.set('A1', 'Parameter')
        self.sheet_0.set('B1', 'Value')
        self.sheet_0.set('C1', 'Unit')
        self.sheet_0.set('D1', 'IsIndependent')

        expression = self.param["geometry"]["diameter_1"]
        self.set_parameter(self.sheet_0, '2', 'diameter_1', expression, 'mm', 'True')

        expression = '= 1.5 * diameter_1'
        self.set_parameter(self.sheet_0, '3', 'diameter_2', expression, 'mm', 'False')

        expression = self.param["geometry"]["small_length"]
        self.set_parameter(self.sheet_0, '4', 'small_length', expression, 'mm', 'True')
        
        expression = '= 0.75 * diameter_1 - small_length'
        self.set_parameter(self.sheet_0, '5', 'straight_inlet', expression, 'mm', 'False')
        
        expression = '= 2.0 * diameter_1 - small_length'
        self.set_parameter(self.sheet_0, '6', 'straight_outlet', expression, 'mm', 'False')
        
        expression = self.param["geometry"]["taper_angle"]
        self.set_parameter(self.sheet_0, '7', 'taper_angle', expression, 'deg', 'True')
        
        expression = '= -(straight_inlet + small_length)'
        self.set_parameter(self.sheet_0, '9', 'x_inlet', expression, 'mm', 'False')
        
        expression = '= -0.5 * small_length'
        self.set_parameter(self.sheet_0, '10', 'x_wall_up', expression, 'mm', 'False')
        
        expression = '= (0.5 * (diameter_2 - diameter_1)) / tan(0.5 * taper_angle) + 0.5 * small_length'
        self.set_parameter(self.sheet_0, '11', 'x_wall_down', expression, 'mm', 'False')
        
        expression = '= x_wall_down + 0.5 * small_length + straight_outlet'
        self.set_parameter(self.sheet_0, '12', 'x_outlet', expression, 'mm', 'False')

        self.sheet_0.recompute()
                       
        return


    def set_parameter(self, sheet, row, name, expression, unit, is_indepemdent):
        sheet.set('A' + row, name)
        sheet.set('B' + row, expression)
        sheet.setAlias('B' + row, name)
        sheet.set('C' + row, unit)
        if is_indepemdent == 'True':
            sheet.set('D' + row, 'True')
            sheet.setBackground('D' + row, self.BLUE)
        else:
            sheet.set('D' + row, 'False')
            sheet.setBackground('D' + row, self.GRAY)

        return


    def create_tapered_cylinder(self):
        sketch_0 = self.body_0.newObject('Sketcher::SketchObject', 'sketch_0')
        sketch_0.Support = (self.doc_0.getObject('XY_Plane'), [''])
        sketch_0.MapMode = 'FlatFace'

        p0 = App.Vector(  0.0, 25.0, 0.0)
        p1 = App.Vector( -5.0, 25.0, 0.0)
        p2 = App.Vector( -5.0,  0.0, 0.0)
        p3 = App.Vector(100.0,  0.0, 0.0)
        p4 = App.Vector(100.0, 50.0, 0.0)
        p5 = App.Vector( 95.0, 50.0, 0.0)

        geo_0 = []
        # index 0 ~ 4
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p0, p1), False))
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p1, p2), False))
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p2, p3), False))
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p3, p4), False))
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p4, p5), False))
        # index 5
        geo_0.append(sketch_0.addGeometry(Part.LineSegment(p5, p0), False))

        con_0 = []
        # index 0 ~ 4
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Coincident',    geo_0[0], self.END, geo_0[1], self.START)))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Coincident',    geo_0[1], self.END, geo_0[2], self.START)))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Coincident',    geo_0[2], self.END, geo_0[3], self.START)))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Coincident',    geo_0[3], self.END, geo_0[4], self.START)))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Coincident',    geo_0[4], self.END, geo_0[5], self.START)))
        # index 5 ~ 9
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Coincident',    geo_0[5], self.END, geo_0[0], self.START)))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Horizontal',    geo_0[0])))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Vertical',      geo_0[1])))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Horizontal',    geo_0[2])))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Vertical',      geo_0[3])))
        # index 10 ~ 14
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Horizontal',    geo_0[4])))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('PointOnObject', geo_0[0], self.START, self.Y_AXIS)))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('PointOnObject', geo_0[1], self.END,   self.X_AXIS)))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('DistanceY',     geo_0[1], self.END,   geo_0[1], self.START, 25.0))) # 'Distance(X|Y)': the sequence of points does matter! 
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('DistanceY',     geo_0[3], self.START, geo_0[3], self.END,   50.0)))
        # index 15 ~ 17
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('DistanceX',     geo_0[0], self.END,   geo_0[0], self.START,  5.0)))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('DistanceX',     geo_0[4], self.END,   geo_0[4], self.START,  5.0)))
        con_0.append(sketch_0.addConstraint(Sketcher.Constraint('Angle',         geo_0[2], self.START, geo_0[5], self.END,    0.31416)))  # rad. not degree

        # note: constraint of DIAMETER_2 MUST BE defined before that of DIAMETER_1
        #       because DIAMETER_2 > DIAMETER_1. (Or the sketch solver may fail to recompute().)
        sht = self.param["name"]["sheet_0"] + '.'
        # sht = '<<' + self.param["name"]["sheet_0"] + '>>.'
        sketch_0.setExpression('Constraints[' + str(con_0[14]) + ']', '0.5 * ' + sht + 'diameter_2')
        sketch_0.setExpression('Constraints[' + str(con_0[13]) + ']', '0.5 * ' + sht + 'diameter_1')
        sketch_0.setExpression('Constraints[' + str(con_0[15]) + ']', sht + 'small_length')
        sketch_0.setExpression('Constraints[' + str(con_0[16]) + ']', sht + 'small_length')
        sketch_0.setExpression('Constraints[' + str(con_0[17]) + ']', '0.5 * ' + sht + 'taper_angle')

        self.doc_0.recompute()

        # create a solid from the sketch
        self.revolution_0 = self.body_0.newObject('PartDesign::Revolution', 'revolution_0')
        self.revolution_0.Profile = sketch_0
        self.revolution_0.ReferenceAxis = (self.doc_0.getObject('X_Axis'), [''])
        self.revolution_0.Angle = 360.0

        self.doc_0.recompute()

        return


    def get_faces_on_which_sketch_will_be_created(self):
        faces_rev_0 = self.revolution_0.Shape.Faces

        x_of_faces = []
        for i in range(len(faces_rev_0)):
            face = faces_rev_0[i]
            x_of_faces.append(face.CenterOfMass.x)

        self.INDEX_MIN_X = np.argmin(x_of_faces)
        self.INDEX_MAX_X = np.argmax(x_of_faces)

        return


    def create_pad_on_revolution(self, SKETCH_LABEL, FACE_INDEX, name_diameter, PAD_LABEL, name_length):
        # make sketch
        sketch = self.body_0.newObject('Sketcher::SketchObject', SKETCH_LABEL)
        # note: the name list of SubObject begins from 'Face1', not 'Face0'
        sketch.Support = (self.revolution_0, 'Face' + str(FACE_INDEX + 1))
        sketch.MapMode = 'FlatFace'

        geo = []
        # index 0
        geo.append(sketch.addGeometry(Part.Circle(App.Vector(0.0, 0.0, 0.0), App.Vector(0, 0, 1), 50.0), False))

        con = []
        # index 0 ~ 1
        con.append(sketch.addConstraint(Sketcher.Constraint('Coincident', geo[0], self.CENTER, self.X_AXIS, 1)))
        # note: 'self.X_AXIS, 1' might indicates the cross point with sketch surface?
        con.append(sketch.addConstraint(Sketcher.Constraint('Diameter', geo[0], 50.0)))

        sht = self.param["name"]["sheet_0"] + '.'
        # sht = '<<' + self.param["name"]["sheet_0"] + '>>.'
        sketch.setExpression('Constraints[1]', sht + name_diameter)

        self.doc_0.recompute()

        # Begin command PartDesign_Pad
        pad = self.body_0.newObject('PartDesign::Pad', PAD_LABEL)
        pad.Profile = sketch
        pad.setExpression('Length', sht + name_length)
        pad.ReferenceAxis = (sketch, ['N_Axis'])

        self.doc_0.recompute()

        return


    ### Remove 3/4 of the whole body (to reduce computational cost in CFD)
    def trim_three_fourths_of_whole_body(self):
        ### create new sketch
        sketch_3 = self.body_0.newObject('Sketcher::SketchObject', 'sketch_3')
        sketch_3.Support = (self.doc_0.getObject('YZ_Plane'), [''])
        sketch_3.MapMode = 'FlatFace'

        geo_3 = []
        # index 0 ~ 2
        geo_3.append(sketch_3.addGeometry(Part.Circle(App.Vector(0.0, 0.0, 0.0), App.Vector(0, 0, 1), 150.0), False))
        geo_3.append(sketch_3.addGeometry(Part.LineSegment(App.Vector(0.0, 0.0, 0), App.Vector(0.0, 150.0, 0.0)), False))
        geo_3.append(sketch_3.addGeometry(Part.LineSegment(App.Vector(0.0, 0.0, 0), App.Vector(150.0, 0.0, 0.0)), False))

        con_3 = []
        # index 0 ~ 4
        con_3.append(sketch_3.addConstraint(Sketcher.Constraint('Coincident',    geo_3[0], self.CENTER,   self.X_AXIS, 1)))
        con_3.append(sketch_3.addConstraint(Sketcher.Constraint('Coincident',    geo_3[1], self.START,  geo_3[0], self.CENTER)))
        con_3.append(sketch_3.addConstraint(Sketcher.Constraint('PointOnObject', geo_3[1], self.END, geo_3[0])))
        con_3.append(sketch_3.addConstraint(Sketcher.Constraint('Vertical',      geo_3[1])))
        con_3.append(sketch_3.addConstraint(Sketcher.Constraint('Coincident',    geo_3[2], self.START,  geo_3[0], self.CENTER)))
        # index 5 ~ 7        
        con_3.append(sketch_3.addConstraint(Sketcher.Constraint('PointOnObject', geo_3[2], self.END, geo_3[0])))
        con_3.append(sketch_3.addConstraint(Sketcher.Constraint('Horizontal',    geo_3[2])))
        con_3.append(sketch_3.addConstraint(Sketcher.Constraint('Radius',        geo_3[0], 50.0)))

        sht = self.param["name"]["sheet_0"] + '.'
        # sht = '<<' + self.param["name"]["sheet_0"] + '>>.'
        sketch_3.setExpression('Constraints[7]', '0.5 * ' + sht + 'diameter_2')
        # a point on the first quadrant of the circle (using Pythagorean theorem):
        diameter_2 = self.sheet_0.get('diameter_2')
        sketch_3.trim(geo_3[0], App.Vector(0.4 * diameter_2, 0.3 * diameter_2, 0.0))

        self.doc_0.recompute()

        ### remove the solid
        pocket_0 = self.body_0.newObject('PartDesign::Pocket', 'pocket_0')
        pocket_0.Profile = sketch_3
        self.doc_0.recompute()
        pocket_0.ReferenceAxis = (sketch_3, ['N_Axis'])
        pocket_0.AlongSketchNormal = 1
        pocket_0.Type = 1  # boring through
        pocket_0.Midplane = 1  # both side

        self.doc_0.recompute()

        return
    

    def save_document(self):
        pwd = os.getcwd()
        file_path_0 = pwd + "/" + self.param["name"]["doc_0"] + ".FCStd"
        self.doc_0.saveAs(file_path_0)

        return
