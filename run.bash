#!/bin/bash

freecadcmd main.py

ls stl | grep '_SI.stl'
if [ $? -eq 1 ]; then
    ls stl/ | sed 's/\.stl//' | xargs -I@ surfaceConvert -scale 0.001 stl/@.stl stl/@_SI.stl
fi
# Now, we get the stl files for OpenFOAM simulation and every patch is named in there.
