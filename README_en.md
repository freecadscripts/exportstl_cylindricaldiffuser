# ExportSTL_ConicalDiffuser



### AUTHOR: Masahiro TANAKA
### DATE: 2022.09.18

## Description

Create ASCII STL file as input geometry of OpenFOAM simulation. In this project, conical diffuser is created. 

The content of the stl file is like this:
```
solid inlet
    facet normal -1 -0 0
        outer loop
            vertex -0.075 0.0222175 0.0035345
            vertex -0.075 0.02 -0
            vertex -0.075 0.0147816 0.0078276
        endloop
    endfacet
    ...
endsolid inlet

solid outlet
    ...
endsolid outlet

solid symmetry
    ...
endsolid symmetry

solid wall_downstream_of_taper
    ...
endsolid wall_downstream_of_taper

solid wall_others
    ...
endsolid wall_others

solid wall_upstream_of_taper
    ...
endsolid wall_upstream_of_taper
```

And the appearance of it is like this:  

![](readme_images/exported_stl.png)

* white  : inlet
* green  : symmetry
* pink   : wall_upstream_of_taper
* blue   : wall_downstream_of_taper
* yellow : wall_others

the pink and blue wall are distinguished from the other walls. these two walls will be used as reference surfaces to culculate the pressure loss(under construction). 


## Requirement

* Linux (Debian, RHEL, SUSE based distro is OK. the others are out of consideration)
* FreeCAD (v0.20 is used)
* OpenFOAM (v2206 is used)


## How To Use

* download this project folder and extruct it in some suitable place.
* open terminal in that folder and type ```./run.bash``` and geometry files will be created.
* to clean up the folder, type ```./clean.bash```


## Derived from:

* https://gitlab.com/freecadscripts/partdesign_cylindricaldiffuser


## Derived into:

* ~~https://gitlab.com/openfoamscripts/fundamentals/cylindricaldiffuser~~
(under construction)


## Reference

* https://wiki.freecadweb.org/Scripting_and_macros
    * https://wiki.freecadweb.org/Scripts
    * https://wiki.freecadweb.org/Sketcher_scripting
    * https://wiki.freecadweb.org/Mesh_Scripting
